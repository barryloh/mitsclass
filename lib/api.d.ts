interface QuestionRequest {
    name: string;
    points: number;
    order?: number;
}
export interface QuestionResponse extends QuestionRequest {
    id: string;
}
export declare function getQuestionsFromFirebase(): Promise<QuestionResponse[]>;
export declare function saveQuestionToFirebase(request: QuestionRequest): Promise<boolean>;
export {};
