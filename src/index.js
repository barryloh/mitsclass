"use strict";
exports.__esModule = true;
var firebase_1 = require("./firebase");
var api_1 = require("./api");
firebase_1.initFirebase();
api_1.getQuestionsFromFirebase()
    .then(function (questions) {
    console.log(questions);
});
// saveQuestionToFirebase({
//     name: 'How old are you?',
//     points: 10
// })
//     .then(isSuccessful => {
//         console.log(`Save successful? ${isSuccessful}`);
//     })
