import * as admin from 'firebase-admin';
import { QuestionResponse } from '../api';

const serviceAccount = require("../../mi-ts-class-firebase.json");

export function saveQuestion(request) {
    return new Promise(async (resolve, reject) => {
        try {
            await admin
                .firestore()
                .collection('questions')
                .doc()
                .set(request);

            resolve();
        } catch (err) {
            console.error(err);
            reject(err);
        }
    })
}

export function getQuestions(): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            const snapshot = await admin
                .firestore()
                .collection('questions')
                .get();

            let questions = [];
            if (snapshot.size > 0) {
                snapshot.forEach(doc => {
                    questions.push({
                        id: doc.id,
                        ...doc.data()
                    });
                });
            }

            resolve(questions);
        } catch (err) {
            console.error(err);
            reject(err);
        }
    })
}

export function initFirebase() {
    return new Promise(async (resolve, reject) => {
        try {
            admin.initializeApp({
                credential: admin.credential.cert(serviceAccount),
                databaseURL: "https://mi-ts-class.firebaseio.com"
            });
            resolve();
        } catch (err) {
            console.error(err);
            reject(err);
        }
    });
}
