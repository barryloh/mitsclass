import { getQuestions, saveQuestion } from './firebase';

interface QuestionRequest {
    name: string;
    points: number;
    order?: number;
}

export interface QuestionResponse extends QuestionRequest {
    id: string;
}

export function getQuestionsFromFirebase(): Promise<QuestionResponse[]> {
    return new Promise(async (resolve, reject) => {
        try {
            const questions = await getQuestions();
            resolve(questions);
        } catch (err) {
            console.error(err);
            reject(err);
        }
    });
}

export function saveQuestionToFirebase(request: QuestionRequest): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            await saveQuestion(request);
            resolve(true);
        } catch (err) {
            console.error(err);
            resolve(false);
        }
    });
};