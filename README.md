# Class Agenda


1. Write a simple file in TS first, then use TSC to convert and run.

2. Write functions with imports in TS, then use TSC to convert and run.

- Create another file called import.ts
- Write function that accepts an object and returns it (data mock in function)
- Call function in index.ts and log out response
- Write another function to return an array (data mock in function)

3. Import tsconfig.json and edit package.json commands.
4. Run command to execute file.
5. Become pro.